// ----------------------------------------------------------------------------
// Copyright (c) 2015 Microsoft Corporation. All rights reserved.
// ----------------------------------------------------------------------------

// This is a base-level Azure Mobile App SDK.
var express = require('express'),
    azureMobileApps = require('azure-mobile-apps'),
    passport = require('passport'),
    FacebookStrategy = require('passport-facebook'),
    bodyParser = require('body-parser');


var port = process.env.PORT || 3000;

// Configure the Facebook strategy for use by Passport.
//
// OAuth 2.0-based strategies require a `verify` function which receives the
// credential (`accessToken`) for accessing the Facebook API on the user's
// behalf, along with the user's profile.  The function must invoke `cb`
// with a user object, which will be set at `req.user` in route handlers after
// authentication.
passport.use(new FacebookStrategy({
  clientID: '129028460992324',
  clientSecret: '58c4625553815db2b83ea9dcd7488da2',
  callbackURL: 'https://menyou-dev.azurewebsites.net/login/facebook/return',
  profileFields: ['id', 'displayName', 'photos', 'email','cover'],    
  enableProof: true
},
  function (accessToken, refreshToken, profile, cb) {
    // In this example, the user's Facebook profile is supplied as the user
    // record.  In a production-quality application, the Facebook profile should
    // be associated with a user record in the application's database, which
    // allows for account linking and authentication with other identity
    // providers.
    console.log("User's FB ID: " + profile.id);
    return cb(null, profile);
  }));


  // Configure Passport authenticated session persistence.
  //
  // In order to restore authentication state across HTTP requests, Passport needs
  // to serialize users into and deserialize users out of the session.  In a
  // production-quality application, this would typically be as simple as
  // supplying the user ID when serializing, and querying the user record by ID
  // from the database when deserializing.  However, due to the fact that this
  // example does not have a database, the complete Facebook profile is serialized
  // and deserialized.
  passport.serializeUser(function (user, cb) {
    cb(null, user);
  });

  passport.deserializeUser(function (obj, cb) {
    cb(null, obj);
  });


// Set up a standard Express app
var app = express();

// Configure view engine to render EJS templates.
app.set('views', __dirname + '/views');
app.set('view engine', 'ejs');

// Using
app.use(require('morgan')('combined'));
// app.use(require('cookie-parser')());
app.use(bodyParser.json());
app.use(require('express-session')({ secret: 'keyboard cat', resave: true, saveUninitialized: true }));

// Initialize Passport and restore authentication state, if any, from the
// session.
app.use(passport.initialize());
app.use(passport.session());



  
  
// load auth
// require('./auth/facebook')(app);    
    
// Load routes
require('./routes/routes')(app);

// If you are producing a combined Web + Mobile app, then you should handle
// anything like logging, registering middleware, etc. here

// Configuration of the Azure Mobile Apps can be done via an object, the
// environment or an auxiliary file.  For more information, see
// http://azure.github.io/azure-mobile-apps-node/global.html#configuration

var mobileApp = azureMobileApps({
    // Explicitly enable the Azure Mobile Apps home page
    homePage: true,
    // Explicitly enable swagger support. UI support is enabled by
    // installing the swagger-ui npm module.
    swagger: true,
    // App will use MS_SqliteFilename or MS_TableConnectionString to choose the SQLite or SQL data provider
    data: {
        dynamicSchema: false
    }
});
  

// Define routes.
app.get('/',
  function (req, res) {
    res.render('home', { user: req.user });
  });

app.get('/login',
  function (req, res) {
    res.render('login');
  });

app.get('/login/facebook',
  passport.authenticate('facebook', { scope: ['user_friends', 'manage_pages'] }));

app.get('/login/facebook/return',
  passport.authenticate('facebook', { successRedirect: '/', failureRedirect: '/login' }));

app.get('/profile',
  require('connect-ensure-login').ensureLoggedIn(),
  function (req, res) {
    res.render('profile', { user: req.user });
  });
  
app.get('/profiledata',
  require('connect-ensure-login').ensureLoggedIn(),
  function (req, res) {
    res.json(req.user);
  });

// Import the files from the tables directory to configure the /tables endpoint
// mobileApp.tables.import('./tables');

// Import the files from the api directory to configure the /api endpoint
// mobileApp.api.import('./api');

console.log('Menu.io app is loaded');


// Initialize the database before listening for incoming requests
// The tables.initialize() method does the initialization asynchronously
// and returns a Promise.
mobileApp.tables.initialize()
    .then(function () {
	app.use(mobileApp);    // Register the Azure Mobile Apps middleware
	app.listen(port);   // Listen for requests
});