module.exports = function (app) {
	
	var qry = require('../controllers/query');
	
	// MEALS - GET API	
	app.get("/meals", function (req, res) {

		var tableConfig = {
			"name": "Meals",
			"schema": "dbo",
			"flavor": "mssql",
			"softDelete": false
		};

		var query = {
			"resultLimit": req.query.$top,
			"selections": req.query.$select,
			"filters": req.query.$filter,
			"ordering": req.query.$orderBy
		};

		var odataQuery = require('azure-odata-sql').format(query, tableConfig);
	
		// Perform query
		qry.executeOdataQuery(res, odataQuery);

	});


	// MEALS - POST API
	app.post("/meals", function (req, res) {
		var query = "INSERT INTO [Meals] (name,createdBy,created,modifiedBy,modified,favorite,rating,category,url,notes) VALUES ('" + req.body.name + "', 'Kameron Berget', GETDATE(), 'Kameron Berget', GETDATE()," + req.body.favorite + "," + req.body.rating + ",'" + req.body.category + "','" + req.body.url + "','" + req.body.notes + "')";
		console.log(query);
		qry.executeQuery(res, query);
	});

	// MEALS - PUT API
	app.put("/meals/:id", function (req, res) {
		var query = "UPDATE [Meals] SET Name='" + req.body.name + "', Favorite=" + req.body.favorite + ", Rating=" + req.body.rating + ", Category='" + req.body.category + "', Url='" + req.body.url + "', Notes='" + req.body.notes + "', modified=GETDATE()  WHERE Id= " + req.params.id;
		console.log(query);
		qry.executeQuery(res, query);
	});

	// MEALS - DELETE API
	app.delete("/meals/:id", function (req, res) {
		// var query = "DELETE FROM [Meals] WHERE Id=" + req.params.id;
		var query = "UPDATE [Meals] SET deleted = 1 WHERE Id= " + req.params.id;
		console.log(query);
		qry.executeQuery(res, query);
	});


	// INGREDIENTS - GET API	
	app.get("/ingredients", function (req, res) {

		var tableConfig = {
			"name": "Ingredients",
			"schema": "dbo",
			"flavor": "mssql",
			"softDelete": false
		};

		var query = {
			"resultLimit": req.query.$top,
			"selections": req.query.$select,
			"filters": req.query.$filter,
			"ordering": req.query.$orderBy
		};

		var odataQuery = require('azure-odata-sql').format(query, tableConfig);
	
		// Perform query
		qry.executeOdataQuery(res, odataQuery);

	});


	// INGREDIENTS - POST API
	app.post("/ingredients", function (req, res) {
		var query = "INSERT INTO [Ingredients] (name,createdBy,created,modifiedBy,modified,category,store,brand,price,isNeeded,size,notes,mealId) VALUES ('" + req.body.name + "', 'Kameron Berget', GETDATE(), 'Kameron Berget', GETDATE(),'" + req.body.category + "','" + req.body.store + "','" + req.body.brand + "'," + req.body.price + ",'" + req.body.isNeeded + "','" + req.body.size + "','" + req.body.notes + "','" + req.body.mealId + "')";
		console.log(query);
		qry.executeQuery(res, query);
	});

	// INGREDIENTS - PUT API
	app.put("/ingredients/:id", function (req, res) {
		var query = "UPDATE [Ingredients] SET Name='" + req.body.name + "', Category='" + req.body.category + "', Store='" + req.body.store + "', Brand='" + req.body.brand + "', Price=" + req.body.price + ", isNeeded=" + req.body.isNeeded + ", Size='" + req.body.size + "', Notes='" + req.body.notes + "', mealId=" + req.body.mealId + ", modified = GETDATE()  WHERE Id= " + req.params.id;
		console.log(query);
		qry.executeQuery(res, query);
	});

	// INGREDIENTS - DELETE API
	app.delete("/ingredients/:id", function (req, res) {
		// var query = "DELETE FROM [Ingredients] WHERE Id=" + req.params.id;
		var query = "UPDATE [Ingredients] SET deleted = 1 WHERE Id= " + req.params.id;
		console.log(query);
		qry.executeQuery(res, query);
	});

	// LISTS - GET API	
	
	
 
	
	app.get("/lists", 
		require('connect-ensure-login').ensureLoggedIn(),
		function (req, res) {
		
		var tableConfig = {
			"name": "Lists",
			"schema": "dbo",
			"flavor": "mssql",
			"softDelete": false
		};

		var query = {
			"resultLimit": req.query.$top,
			"selections": req.query.$select,
			"filters": req.query.$filter,
			"ordering": req.query.$orderBy
		};

		var odataQuery = require('azure-odata-sql').format(query, tableConfig);
	
		// Perform query
		qry.executeOdataQuery(res, odataQuery);

	});


	// LISTS - POST API
	app.post("/lists", function (req, res) {
		var query = "INSERT INTO [lists] (name,createdBy,created,modifiedBy,modified,week,totalCost) VALUES ('" + req.body.name + "', 'Kameron Berget', GETDATE(), 'Kameron Berget', GETDATE(),'" + req.body.week + "'," + req.body.totalCost + ")";
		console.log(query);
		qry.executeQuery(res, query);
	});

	// LISTS - PUT API
	app.put("/lists/:id", function (req, res) {
		var query = "UPDATE [lists] SET Name='" + req.body.name + "', Week='" + req.body.week + "', TotalCost=" + req.body.totalCost + ", modified = GETDATE()  WHERE Id= " + req.params.id;
		console.log(query);
		qry.executeQuery(res, query);
	});

	// LISTS - DELETE API
	app.delete("/lists/:id", function (req, res) {
		// var query = "DELETE FROM [lists] WHERE Id=" + req.params.id;
		var query = "UPDATE [lists] SET deleted = 1 WHERE Id= " + req.params.id;
		console.log(query);
		qry.executeQuery(res, query);
	});
};