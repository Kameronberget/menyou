var sql = require('mssql'),
	database = require('.././config/dbconfig');

exports.executeOdataQuery = function(res, odataQuery) {
	
	var sqlQry = odataQuery[0].sql;
    console.log(odataQuery[0].sql);
	
	// connect to your database
	sql.connect(database.config, function (err) {
	
		if (err) console.log(err);

		// create Request object
		var request = new sql.Request();  

		for (var i = 0; i < odataQuery[0].parameters.length; i++) {
			console.log(odataQuery[0].parameters[i]);
			request.input(odataQuery[0].parameters[i].name, odataQuery[0].parameters[i].value);
		}   
		// query to the database and get the records
		request.query(sqlQry, function (err, recordset) {
			
			if (err) {
			   res.json(err.message);
			   console.log(err);
			} 
							
			// send records as a rnse
			var data = {
				"results" : recordset
			};
			res.json(data);
			
		});
	});
};

exports.executeQuery = function(res, query) {
	sql.connect(database.config, function (err) {
		if (err) {   
			console.log("Error while connecting database :- " + err);
			res.send(err);
		}
		else {
			// create Request object
			var request = new sql.Request();
			// query to the database
			request.query(query, function (err, recordset) {
				if (err) {
					console.log("Error while querying database :- " + err);
					res.send(err);
				}
				else {
					// send records as
					var data = {
						"results"   : recordset
					};
					res.json(data);
				}
			});
		}
	});	
};